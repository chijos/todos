import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';

import { MaterialModule } from '../material/material.module';

import { TodoComponent } from './containers/todo/todo.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { effects } from '../state';

const ROUTES = [{ path: '', component: TodoComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    EffectsModule.forFeature(effects)
  ],
  declarations: [TodoComponent, TodoListComponent, AddTodoComponent, TodoItemComponent]
})
export class TodoModule {}
