import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TodoItem } from '../../../models/TodoItem.model';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent {
  @Output()
  addTodo = new EventEmitter<TodoItem>();

  addTodoForm = new FormGroup({
    name: new FormControl('', [Validators.required])
  });

  submit() {
    const todo = <TodoItem>{
      id: -1,
      name: this.addTodoForm.value.name,
      isComplete: false
    };
    this.addTodo.emit(todo);
    this.addTodoForm.reset();
  }
}
