import { Component, Input, Output, EventEmitter } from '@angular/core';

import { TodoItem } from '../../../models/TodoItem.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {
  @Input()
  todoItems: TodoItem[];

  @Output()
  toggleTodoItem = new EventEmitter<TodoItem>();

  toggleTodo(todoItem: TodoItem) {
    this.toggleTodoItem.emit(todoItem);
  }
}
