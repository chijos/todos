import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TodoItem } from '../../../models/TodoItem.model';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
  @Input()
  todoItem: TodoItem;

  @Output()
  toggleTodoItem = new EventEmitter<TodoItem>();

  toggleTodo(todoItem: TodoItem) {
    this.toggleTodoItem.emit(todoItem);
  }
}
