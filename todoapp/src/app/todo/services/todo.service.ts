import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { TodoItem } from '../../models/TodoItem.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private todoApiUrl = 'api/todo';

  constructor(private http: HttpClient) {}

  getTodos(): Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(this.todoApiUrl);
  }

  addTodo(todo: TodoItem): Observable<TodoItem> {
    return this.http.post<TodoItem>(this.todoApiUrl, todo);
  }

  toggleTodo(todo: TodoItem): Observable<TodoItem> {
    todo.isComplete = !todo.isComplete;
    return this.http.put<TodoItem>(`${this.todoApiUrl}/${todo.id}`, todo);
  }
}
