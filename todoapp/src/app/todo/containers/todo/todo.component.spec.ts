import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '../../../material/material.module';
import { TodoComponent } from './todo.component';
import { TodoListComponent } from '../../components/todo-list/todo-list.component';
import { AddTodoComponent } from '../../components/add-todo/add-todo.component';
import { TodoItemComponent } from '../../components/todo-item/todo-item.component';
import { Store } from '@ngrx/store';

describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Store,
          useValue: {
            pipe: () => {},
            dispatch: action => {}
          }
        }
      ],
      imports: [ReactiveFormsModule, NoopAnimationsModule, HttpClientTestingModule, MaterialModule],
      declarations: [AddTodoComponent, TodoListComponent, TodoItemComponent, TodoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
