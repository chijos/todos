import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { State } from '../../../state';
import { TodoItem } from '../../../models/TodoItem.model';
import { LoadTodos, AddTodo, ToggleTodo } from '../../../state/todos/todos.actions';
import { getTodos } from '../../../state/todos/todos.selectors';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todoItems$: Observable<TodoItem[]>;

  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.todoItems$ = this.store.pipe(select(getTodos));
    this.loadTodos();
  }

  loadTodos() {
    this.store.dispatch(new LoadTodos());
  }

  addTodo(todo: TodoItem) {
    this.store.dispatch(new AddTodo(todo));
  }

  toggleTodo(todo: TodoItem) {
    this.store.dispatch(new ToggleTodo(todo));
  }
}
