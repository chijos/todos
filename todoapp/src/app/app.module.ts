import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { MaterialModule } from './material/material.module';
import * as fromTodos from './state/todos/todos.reducer';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';

const ROUTES = [
  { path: '', pathMatch: 'full', redirectTo: 'todos' },
  { path: 'todos', loadChildren: './todo/todo.module#TodoModule' }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({ todos: fromTodos.reducer }),
    StoreDevtoolsModule.instrument({
      name: 'Todos App',
      logOnly: environment.production
    }),
    MaterialModule,
    RouterModule.forRoot(ROUTES),
    HttpClientModule,
    EffectsModule.forRoot([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
