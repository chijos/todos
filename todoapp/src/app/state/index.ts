import * as fromTodos from './todos/todos.reducer';
import { TodosEffects } from './todos/todos.effects';

export interface State {
  todos: fromTodos.State;
}

export const effects: any[] = [TodosEffects];
