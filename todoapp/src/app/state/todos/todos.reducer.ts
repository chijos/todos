import { TodosActionTypes } from './todos.actions';
import { TodoItem } from '../../models/TodoItem.model';

export interface State {
  todos: TodoItem[];
  error: any;
}

export const initialState: State = {
  todos: [],
  error: null
};

export function reducer(state = initialState, action): State {
  switch (action.type) {
    case TodosActionTypes.LoadTodosSuccess:
      return {
        ...state,
        todos: action.payload
      };

    case TodosActionTypes.LoadTodosFail:
      return {
        ...state,
        error: action.payload
      };

    case TodosActionTypes.AddTodoSuccess:
      return {
        ...state,
        todos: [...state.todos, action.payload]
      };

    case TodosActionTypes.AddTodoFail:
      return {
        ...state,
        error: action.payload
      };

    case TodosActionTypes.ToggleTodoSuccess:
      return {
        ...state,
        todos: [
          ...state.todos.map((item, index) => {
            if (item.id !== action.payload.id) {
              return item;
            }
            return {
              ...item,
              ...action.payload
            };
          })
        ]
      };

    case TodosActionTypes.ToggleTodoFail:
      return {
        ...state,
        error: action.payload
      };

    default:
      return state;
  }
}
