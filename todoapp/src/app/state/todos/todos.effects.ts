import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';

import { TodoService } from '../../todo/services/todo.service';
import {
  TodosActionTypes,
  LoadTodos,
  LoadTodosSuccess,
  LoadTodosFail,
  AddTodoSuccess,
  AddTodoFail,
  AddTodo,
  ToggleTodo,
  ToggleTodoSuccess,
  ToggleTodoFail
} from '../todos/todos.actions';
import { TodoItem } from '../../models/TodoItem.model';
import { of } from 'rxjs';

@Injectable()
export class TodosEffects {
  constructor(private actions$: Actions, private todoService: TodoService) {}

  @Effect()
  loadTodos$ = this.actions$.pipe(
    ofType(TodosActionTypes.LoadTodos),
    switchMap((action: LoadTodos) =>
      this.todoService.getTodos().pipe(
        map((todos: TodoItem[]) => new LoadTodosSuccess(todos)),
        catchError(error => of(new LoadTodosFail(error)))
      )
    )
  );

  @Effect()
  addTodo$ = this.actions$.pipe(
    ofType(TodosActionTypes.AddTodo),
    switchMap((action: AddTodo) =>
      this.todoService.addTodo(action.payload).pipe(
        map((todo: TodoItem) => new AddTodoSuccess(todo)),
        catchError(error => of(new AddTodoFail(error)))
      )
    )
  );

  @Effect()
  toggleTodo$ = this.actions$.pipe(
    ofType(TodosActionTypes.ToggleTodo),
    switchMap((action: ToggleTodo) =>
      this.todoService.toggleTodo(action.payload).pipe(
        map((todo: TodoItem) => new ToggleTodoSuccess(todo)),
        catchError(error => of(new ToggleTodoFail(error)))
      )
    )
  );
}
