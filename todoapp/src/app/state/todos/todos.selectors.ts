import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './todos.reducer';

export const getTodosState = createFeatureSelector<State>('todos');

export const getTodos = createSelector(getTodosState, (state: State) => state.todos);
