import { TodoItem } from '../../models/TodoItem.model';
import { Action } from '@ngrx/store';

export enum TodosActionTypes {
  LoadTodos = '[Todos Page] Load todos',
  LoadTodosSuccess = '[Todos API] Load todos success',
  LoadTodosFail = '[Todos API] Load todos fail',
  AddTodo = '[Todos Page] Add todo',
  AddTodoSuccess = '[Todos API] Add todo success',
  AddTodoFail = '[Todos API] Add todo fail',
  ToggleTodo = '[Todos Page] Toggle todo',
  ToggleTodoSuccess = '[Todos API] Toggle todo success',
  ToggleTodoFail = '[Todos API] Toggle todo fail'
}

export class LoadTodos implements Action {
  readonly type: string = TodosActionTypes.LoadTodos;
}

export class LoadTodosSuccess implements Action {
  readonly type: string = TodosActionTypes.LoadTodosSuccess;

  constructor(public payload: TodoItem[]) {}
}

export class LoadTodosFail implements Action {
  readonly type: string = TodosActionTypes.LoadTodosFail;

  constructor(public payload: any) {}
}

export class AddTodo implements Action {
  readonly type: string = TodosActionTypes.AddTodo;

  constructor(public payload: TodoItem) {}
}

export class AddTodoSuccess implements Action {
  readonly type: string = TodosActionTypes.AddTodoSuccess;

  constructor(public payload: TodoItem) {}
}

export class AddTodoFail implements Action {
  readonly type: string = TodosActionTypes.AddTodoFail;

  constructor(public payload: any) {}
}

export class ToggleTodo implements Action {
  readonly type: string = TodosActionTypes.ToggleTodo;

  constructor(public payload: TodoItem) {}
}

export class ToggleTodoSuccess implements Action {
  readonly type: string = TodosActionTypes.ToggleTodoSuccess;

  constructor(public payload: TodoItem) {}
}

export class ToggleTodoFail implements Action {
  readonly type: string = TodosActionTypes.ToggleTodoFail;

  constructor(public payload: any) {}
}

export type TodosActions =
  | LoadTodos
  | LoadTodosSuccess
  | LoadTodosFail
  | AddTodo
  | AddTodoSuccess
  | AddTodoFail
  | ToggleTodo
  | ToggleTodoSuccess
  | ToggleTodoFail;
